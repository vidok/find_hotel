require 'rails_helper'

describe CreateBooking do
  describe '#call' do
    let(:hotel) { Hotel.create(google_id: 'google-id', city: 'Test', name: 'Test name') }
    let(:user) { User.create(email: 'email@test.com', name: 'test name') }
    let(:form) { BookingForm.new( { started_at: Date.today, ended_at: Date.tomorrow } ) }
    let(:result) { CreateBooking.call(form: form, hotel: hotel, user: user) }

    context 'when positive data' do
      it 'returns success result' do
        expect(result).to be_success
      end

      it 'creates booking' do
        expect{result}.to change{Booking.count}.from(0).to(1)
      end
    end

    context 'when negative data' do
      before(:example) do
        Booking.create(hotel: hotel, user: user, started_at: Date.today, ended_at: Date.tomorrow)
      end
      it 'does not create booking' do
        expect{result}.to_not change{User.count}
      end

      it 'returns fail result' do
        expect(result).to be_failure
      end

      it 'has errors' do
        expect(result.errors).not_to be_empty
      end
    end
  end
end
