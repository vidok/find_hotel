require 'rails_helper'

describe ValidateForm do
  describe '#call' do
    let(:result) { ValidateForm.call(booking_params: params) }

    context 'when success' do
      let(:params) {{
        hotel_id: 1,
        started_at: Date.today,
        ended_at: Date.tomorrow,
        user_name: 'test',
        user_email: 'email@test.com'
      }}

      it 'returns success result' do
        expect(result).to be_success
      end

      it 'returns BookingForm' do
        expect(result.form).to be_a_kind_of(BookingForm)
      end

      it 'returns form without errors' do
        expect(result.errors).to be_nil
      end
    end

    context 'when failed' do
      let(:params) {{
        hotel_id: 1,
        started_at: Date.today,
        ended_at: Date.tomorrow,
        user_name: 'test',
        user_email: 'invalid email'
      }}

      it 'returns failed result' do
        expect(result).to be_failure
      end

      it 'returns BookingForm' do
        expect(result.form).to be_a_kind_of(BookingForm)
      end

      it 'returns form witht errors' do
        expect(result.errors).not_to be_empty
      end
    end
  end
end
