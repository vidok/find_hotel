require 'rails_helper'

describe EnsureUser do
  describe '#call' do
    let(:result) { EnsureUser.call(form: form) }

    context 'when not exists user' do
      let(:form) { BookingForm.new( { user_email: 'email@test.com', user_name: 'test name' } ) }

      it 'creates user' do
        expect{result}.to change{User.count}.from(0).to(1)
      end
    end

    context 'when exists user' do
      let(:form) { BookingForm.new( { user_email: 'email@test.com', user_name: 'test name' } ) }

      it 'does not create user' do
        User.create(email: 'email@test.com', name: 'test name')
        expect{result}.to_not change{User.count}
      end
    end
  end
end
