require 'rails_helper'

describe EnsureHotel do
  describe '#call' do
    let(:result) { EnsureHotel.call(google_id: 1) }
    let(:hotel) { double('Hotel') }

    context 'when not exists hotel' do
      it 'returns fail result' do
        allow(Hotel).to receive(:ensure).and_return(nil)

        expect(result).to be_failure
      end
    end

    context 'when exists hotel' do
      before(:example) do
        allow(Hotel).to receive(:ensure).and_return(hotel)
      end

      it 'returns success response' do
        expect(result).to be_success
      end

      it 'returns hotel' do
        expect(result.hotel).to eq(hotel)
      end
    end
  end
end
