require 'rails_helper'

describe Hotel, type: :model do
  describe '#ensure' do
    before(:example) do
      place = instance_double('Google_spot', city: 'Test', name: 'Test name', place_id: 'test_1')
      allow(GooglePlaces::Spot).to receive(:find).and_return(place)
    end

    it 'creates place' do
      expect{Hotel.ensure(google_id: 'test_1')}.to change{Hotel.count}.from(0).to(1)
    end

    it 'creates place with special google_id' do
      hotel = Hotel.ensure(google_id: 'test_1')
      expect(hotel.google_id).to eq('test_1')
    end

    it 'does not create place' do
      Hotel.create(city: 'Test', name: 'Test name', google_id: 'test_1')

      hotel = Hotel.ensure(google_id: 'test_1')
      expect{Hotel.ensure(google_id: 'test_1')}.not_to change{Hotel.count}
    end

    it 'raises exception' do
      allow(GooglePlaces::Spot).to receive(:find).and_raise(GooglePlaces::InvalidRequestError.new('Error'))
      expect(Hotel.ensure(google_id: 'test_1')).to be_nil
    end
  end
end
