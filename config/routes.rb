Rails.application.routes.draw do
  root to: 'home#index'

  resources :hotels, only: [:new, :create] do
    resources :bookings, only: [:new, :create]
  end

  mount Bookings::API => '/api'
end
