class CreateHotels < ActiveRecord::Migration[5.0]
  def change
    create_table :hotels do |t|
      t.string :name
      t.string :city
      t.text :google_id
      t.timestamps
    end

    add_index :hotels, :google_id, unique: true
  end
end
