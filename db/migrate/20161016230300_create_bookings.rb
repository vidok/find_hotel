class CreateBookings < ActiveRecord::Migration[5.0]
  def change
    create_table :bookings do |t|
      t.integer :hotel_id
      t.integer :user_id
      t.date :started_at
      t.date :ended_at
      t.timestamps
    end

    add_index :bookings, :user_id
  end
end
