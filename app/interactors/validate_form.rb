class ValidateForm
  include Interactor

  def call
    form = BookingForm.new(context.booking_params)
    context.form = form
    unless form.valid?
      context.fail!(errors: form.errors)
    end
  end
end
