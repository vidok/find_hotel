class MakeBooking
  include Interactor::Organizer

  organize ValidateForm, EnsureUser, CreateBooking
end
