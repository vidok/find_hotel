class EnsureUser
  include Interactor

  def call
    form = context.form
    user = User.find_by_email(form.user_email)
    unless user
      user = User.create(email: form.user_email, name: form.user_name)
    end

    context.user = user
  end
end
