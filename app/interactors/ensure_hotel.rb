class EnsureHotel
  include Interactor

  def call
    hotel = Hotel.ensure(google_id: context.google_id)
    context.fail! unless hotel
    context.hotel = hotel
  end
end
