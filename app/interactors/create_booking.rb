class CreateBooking
  include Interactor

  def call
    form = context.form
    hotel = context.hotel
    booking = hotel.bookings.
      where(user: context.user).
      where("(started_at, ended_at) OVERLAPS (DATE ?, DATE ?)", form.started_at, form.ended_at).
      limit(1)

    if booking.exists?
      context.fail!(errors: ['You allready have booking on this dates'])
    else
      context.booking = hotel.bookings.create(
        user: context.user,
        started_at: form.started_at,
        ended_at: form.ended_at
      )
   end
  end
end
