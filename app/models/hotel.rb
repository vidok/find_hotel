class Hotel < ApplicationRecord
  has_many :bookings

  def self.ensure google_id:
    hotel = find_by_google_id(google_id)
    return hotel if hotel

    google_place = find_google_place google_id
    return unless google_place

    create(
      city: google_place.city,
      name: google_place.name,
      google_id: google_place.place_id
    )
  end

  def self.find_google_place(google_id)
    GooglePlaces::Spot.find(google_id, ENV['GOOGLE_PLACES_API_KEY'])
  rescue GooglePlaces::InvalidRequestError => e
    logger.error "Can't find google place with id=#{google_id}"
    return nil
  end
end
