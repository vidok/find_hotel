class BookingForm
  include ActiveModel::Model
  include Virtus.model
  include Virtus::Multiparams

  # Attributes (DSL provided by Virtus)
  attribute :hotel_id, String
  attribute :user_email, String
  attribute :user_name, String
  attribute :started_at, Date
  attribute :ended_at, Date

  # Validations
  validates :hotel_id,
            :user_name,
            :user_email,
            :started_at,
            :ended_at,
            presence: true

  validates :user_email, email_format: { message: 'does not look like an email address' }

  validate :started_at_cannot_be_greater_than_ended_at
  validate :started_at_cannot_be_in_the_past

  private

  def started_at_cannot_be_greater_than_ended_at
    if started_at >= ended_at
      errors.add(:started_at, 'Started at can not be greater than ended at')
    end
  end

  def started_at_cannot_be_in_the_past
    if started_at < Date.today
      errors.add(:started_at, 'Started at can not be in the past')
    end
  end
end
