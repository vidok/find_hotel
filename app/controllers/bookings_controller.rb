class BookingsController < ApplicationController
  before_filter :ensure_hotel

  def new
    @booking_form = BookingForm.new(hotel_id: @hotel.id)
  end

  def create
    result = MakeBooking.call(booking_params: booking_params, hotel: @hotel)
    if result.success?
      redirect_to root_url
    else
      @booking_form = result.form
      @errors = result.errors
      render :new
    end
  end

  private

  def booking_params
    params.require(:booking_form).permit(
      :started_at,
      :ended_at,
      :user_name,
      :user_email,
      :hotel_id
    )
  end

  def ensure_hotel
    result = EnsureHotel.call(google_id: params[:hotel_id])
    if result.success?
      @hotel = result.hotel
    else
      raise ActionController::RoutingError.new('Hotel not Found')
    end
  end
end
