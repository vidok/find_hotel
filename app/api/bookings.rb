module Bookings
  class API < Grape::API
    version 'v1', using: :path
    format :json

    resource :bookings do
      Rails.logger.debug ' adad asd'
      desc 'Return a bookings list.'
      get do
        present Booking.includes(:hotel, :user), with: Entities::Booking
      end

      mount Users::API
    end
  end
end
