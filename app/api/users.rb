module Users
  class API < Grape::API
    params do
      requires :id, type: Integer
    end

    route_param :id, requirements: { id: /[0-9]*/ } do
      desc 'Return all bookings for special user'
      get do
        user = User.find(params[:id])
        present(user.bookings, with: Entities::Booking)
      end
    end
  end
end
