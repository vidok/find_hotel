module Entities
  class Booking < Grape::Entity
    expose :id
    expose :city do |status, _|
      status.hotel.city
    end

    expose :hotel do |status, _|
      status.hotel.name
    end

    expose :started_at
    expose :ended_at
    expose :user, using: Entities::User
  end
end
